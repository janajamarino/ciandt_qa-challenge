Feature: Busca no Banco de Questões por categoria


    Scenario: Busca lista de questões por categoria
        Given Que eu acesso ágina de busca do opentdb
        And digito 'Science: Computers' no campo de busca
        And Seleciono o método de busca como 'Category'
        When clico no botão de buscar
        Then visualizo uma lista de questões
        And há 25 ítens por página
        And O ID do último elemento da primeira é maior que o primeiro elemento da segunda página


    Scenario: Busca por categoria inexistente
        Given que navego para a página de busca do banco de questões
        And digito 'Multiple' no campo de busca
        And Seleciono o método de busca como 'Category'
        When clico no botão de buscar
        Then visualizo uma mensagem de erro com o texto 'No questions found'

    Scenario: Busca lista de questões por todas as categorias
        Given que navego para a página de busca do banco de questões
        And Seleciono o método de busca como 'Category'
        When clico no botão de buscar
        Then visualizo uma lista com todas as questões agrupadas por categoria
