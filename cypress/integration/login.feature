Feature: Login, cadatro e recuperação de senha


    Scenario: Cadastrar novo usuário
        Given acesso o site opentdb
        And navego até a página de login
        When clico em "Register"
        And Informo meus dados de nome, senha, e e-mail
        And clico em "Register"
        Then devo visualizar uma mensagem de sucesso "Registro com sucesso"

    Scenario: Logar com credenciais válidas
        Given acesso o site opentdb
        And navego até a página de login
        When eu informo minhas credenciais
        And clico no botão 'sign in'
        Then devo visualizar a área logada

    Scenario: Logar com credenciais inválidas
        Given acesso o site opentdb
        And navego até a página de login
        When eu informo minhas credenciais com erro
        And clico no botão 'sign in'
        Then devo visualizar uma mensagem de erro 'ERROR! Logging In Failed.'

    Scenario: Recuperar a senha
        Given acesso o site opentdb
        And navego até a página de login
        When clico em 'Forgot Password'
        And informo meu e-mail
        And clico em 'Send password reset email'
        Then devo visualizar uma mensagem '...' Preciso do captcha

