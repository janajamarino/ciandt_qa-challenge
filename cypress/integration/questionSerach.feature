Feature: Busca no Banco de Questões por questão

    Scenario: Busca por questão inexistente
        Given Que eu acesso ágina de busca do opentdb
        And digito 'Science: Computers' no campo de busca
        And Seleciono o método de busca como 'Question'
        When clico no botão de buscar
        Then visualizo uma mensagem de erro com o texto 'No questions found'       

    Scenario: Busca por questão específica
        Given que navego para a página de busca do banco de questões
        And digito 'when did the rapper Eazy-E die?' no campo de busca
        And Seleciono o método de busca como 'Question'
        When clico no botão de buscar
        Then visualizo uma lista com todas as questões que conenham o termo buscado

        Scenario: Busca por todas as questões
        Given que navego para a página de busca do banco de questões
        And Seleciono o método de busca como 'Question'
        When clico no botão de buscar
        Then visualizo uma lista com todas as questões ordenado por ID descresccente
       