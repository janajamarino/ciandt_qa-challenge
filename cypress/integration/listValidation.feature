Feature: Validação critérios de formato da lista

 
    Scenario: validar Ordenacao
        Given que eu efetuei uma busca
        And visualizei uma lista de questões
        Then visualizo uma mensagem de erro com o texto 'No questions found'

    Scenario: validar Numero De Itens Por Pagina
        Given que eu visualizo uma lista de questões
        When conto quantas questões aparecem na página
        Then Eu devo ter 25 questões na página

    Scenario: validar Paginacao
        Given que eu visualizo uma lista de qustões
        And observo que há mais páginas nesta lista
        When navego para a próxima página
        Then que o ID do primeiro elemento da lista é menor que último elemento da página anterior
