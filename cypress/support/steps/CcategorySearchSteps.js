/* global Given, When , Then */

import CategorySearchPage from '../pageobjects/CategorySearchPage'

const categorySearchPage = new CategorySearchPage


Given("Que eu acesso ágina de busca do opentdb", () => {
    categorySearchPage.acessarSite();
})

And("digito 'Science: Computers' no campo de busca", () => {
    categorySearchPage.digitartexto();
})

And("Seleciono o método de busca como 'Category", () => {
    categorySearchPage.selecionarCategory();
})

When("clico no botão de buscar",() => {
    categorySearchPage.clicarBotaodeSearch();
})


Then("visualizo uma uma lista de de questões'", () => {
    categorySearchPage.listaDeQuestões();
})