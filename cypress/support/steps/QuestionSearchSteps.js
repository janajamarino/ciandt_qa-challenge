/* global Given, When , Then */

import QuestionSearchPage from '../pageobjects/QuestionSearchPage'

const questionSearchPage = new QuestionSearchPage


Given("Que eu acesso ágina de busca do opentdb", () => {
    questionsearchPage.acessarSite();
})
And("digito 'Science: Computers' no campo de busca", () => {
    questionsearchPage.digitartexto();
})

And("Seleciono o método de busca como 'Question", () => {
    questionsearchPage.selecionarQuestion();
})

When("clico no botão de buscar",() => {
    questionsearchPage.clicarBotaodeSearch();
})


Then("visualizo uma uma lista de de questões'", () => {
    questionsearchPage.listaDeQuestões();
})