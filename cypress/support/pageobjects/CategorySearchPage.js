/// <reference types="Cypress/">

import SearchElements from '../elements/SearchElements'

const searchElements = new SearchElements
const url = Cypress.config("browseUrl")

class CategorySearchPage{
    //Acessar Site opentdc
    acessarSite(){
        cy.visit(url)
    }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       

    //Navegar para página de pesquisa
    NavegarBrowse(){
        cy.get(searchElements.browseMenu(should('contain', searchElements.browseMenuname))).click
    }

    // digitar termo a se pesquisar
    digitartexto(){
        cy.get(searchElements.campoInput()).type('Science: Computers')
    }
    
    //selecionarCategory                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    selecionarQuestion(){
        cy.get(searchElements.tipo()).contais(searchElements.categoria).click()

    }

        
    //Clicar botão de busca
    clicarBotaodeSearch(){
        cy.get(searchElements.botaoSearch()).click()
        }

        
    //Visualusar mensagem de erro
    visualizarMensagemDeErro(){
        cy.get(searchElements.BoxDeAlerta()).should('contain', MensagemDeErro()) 
    }
}

export default CategorySearchPage



