class SearchElements {
    browseMenu = () => {return 'btn btn-primary'}
    browseMenuName = () => {return 'Browse'}
    botaoSearch = () => {return '.btn btn-sm btn-default' }
    campoInput = () => {return '#query'}
    tipoBusca = () => {return '#type'}
    categoria = () => {return 'Category'}
    questao = () => {return 'Question'}
    table = () => { return '.table table-bordered' }  
    BoxDeAlerta = () => { return '.alert alert-danger' }
    MensagemDeErro = () => {return 'No questions found.'}

}

export default SearchElements;